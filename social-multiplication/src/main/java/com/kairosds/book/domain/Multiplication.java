package com.kairosds.book.domain;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public final class Multiplication {

    @Id
    @GeneratedValue
    @Column(name = "MULTIPLICATION_ID")
    private Long id;

    // Both factors
    private int factorA;
    private int factorB;

    // Empty constructor for JSON/JPA
    public Multiplication() {
        this(0, 0);
    }

    public Multiplication(int factorA, int factorB) {
        this.factorA = factorA;
        this.factorB = factorB;
    }

    public Long getId() {
        return id;
    }

    public int getFactorA() {
        return factorA;
    }

    public int getFactorB() {
        return factorB;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Multiplication that = (Multiplication) o;

        return new EqualsBuilder().append(factorA, that.factorA).append(factorB, that.factorB).append(id, that.id)
                                  .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(id).append(factorA).append(factorB).toHashCode();
    }


    public static class Builder {

        private final Multiplication object;

        public Builder() {
            object = new Multiplication();
        }


        public Builder withId(Long value) {
            object.id = value;
            return this;
        }

        public Builder withFactorA(int value) {
            object.factorA = value;
            return this;
        }

        public Builder withFactorB(int value) {
            object.factorB = value;
            return this;
        }

        public Multiplication build() {
            return object;
        }
    }
}
