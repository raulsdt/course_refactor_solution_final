package com.kairosds.book;

import com.kairosds.book.domain.User;
import org.springframework.stereotype.Service;

@Service
public class UserQueryService {

    private UserRepository userRepository;

    public UserQueryService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User getUser(final Long userId){
        return userRepository.findById(userId).orElseThrow(IllegalArgumentException::new);
    }
}
