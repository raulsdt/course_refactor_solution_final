package com.kairosds.book;

public interface JournalProvider {

    void send(final Long multiplicationResultAttemptId, final Long userId, final boolean correct);
}
