package com.kairosds.book;

import com.kairosds.book.domain.MultiplicationResultAttempt;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MultiplicationQueryService {

    public MultiplicationQueryService(MultiplicationResultAttemptRepository attemptRepository) {
        this.attemptRepository = attemptRepository;
    }

    private MultiplicationResultAttemptRepository attemptRepository;

    public List<MultiplicationResultAttempt> getStatsForUser(final String userAlias) {
        return attemptRepository.findTop5ByUserAliasOrderByIdDesc(userAlias);
    }

    public MultiplicationResultAttempt getResultById(final Long resultId) {
        return attemptRepository.findById(resultId).orElseThrow(
                () -> new IllegalArgumentException("The requested resultId [" + resultId + "] does not exist."));
    }
}
