package com.kairosds.book.adapter.rest;

import com.kairosds.book.MultiplicationCommandService;
import com.kairosds.book.MultiplicationQueryService;
import com.kairosds.book.adapter.rest.dto.MultiplicationResultAttemptResponse;
import com.kairosds.book.domain.MultiplicationResultAttempt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/results")
public class MultiplicationResultAttemptController {

    private final MultiplicationQueryService multiplicationQueryService;
    private final MultiplicationCommandService multiplicationCommandService;
    private final MultiplicationResultAttemptToMultiplciationResultAttemptResponseConverter multiplicationResultAttemptConverter;


    @Autowired
    MultiplicationResultAttemptController(final MultiplicationQueryService multiplicationQueryService,
            MultiplicationCommandService multiplicationCommandService,
            MultiplicationResultAttemptToMultiplciationResultAttemptResponseConverter multiplicationResultAttemptConverter) {
        this.multiplicationQueryService = multiplicationQueryService;
        this.multiplicationCommandService = multiplicationCommandService;
        this.multiplicationResultAttemptConverter = multiplicationResultAttemptConverter;
    }

    @PostMapping
    ResponseEntity<MultiplicationResultAttemptResponse> postResult(
            @RequestBody MultiplicationResultAttempt multiplicationResultAttempt) {
        return ResponseEntity.ok(multiplicationResultAttemptConverter.convert(
                multiplicationCommandService.checkAttempt(multiplicationResultAttempt)));
    }

    @GetMapping
    ResponseEntity<List<MultiplicationResultAttemptResponse>> getStatistics(@RequestParam("alias") String alias) {
        return ResponseEntity.ok(multiplicationQueryService.getStatsForUser(alias).stream()
                                                           .map(multiplicationResultAttemptConverter::convert)
                                                           .collect(Collectors.toList()));
    }

    @GetMapping("/{resultId}")
    ResponseEntity<MultiplicationResultAttemptResponse> getResultById(final @PathVariable("resultId") Long resultId) {
        return ResponseEntity
                .ok(multiplicationResultAttemptConverter.convert(multiplicationQueryService.getResultById(resultId)));
    }

}
