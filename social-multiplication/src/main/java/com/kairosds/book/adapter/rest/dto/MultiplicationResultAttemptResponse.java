package com.kairosds.book.adapter.rest.dto;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

public class MultiplicationResultAttemptResponse {

    private Long id;
    private UserResponse user;
    private MultiplicationResponse multiplication;
    private int resultAttempt;
    private boolean correct;

    public MultiplicationResultAttemptResponse() {
    }

    public MultiplicationResultAttemptResponse(Long id, UserResponse user, MultiplicationResponse multiplication,
            int resultAttempt, boolean correct) {
        this.id = id;
        this.user = user;
        this.multiplication = multiplication;
        this.resultAttempt = resultAttempt;
        this.correct = correct;
    }

    public Long getId() {
        return id;
    }

    public UserResponse getUser() {
        return user;
    }

    public MultiplicationResponse getMultiplication() {
        return multiplication;
    }

    public int getResultAttempt() {
        return resultAttempt;
    }

    public boolean isCorrect() {
        return correct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        MultiplicationResultAttemptResponse that = (MultiplicationResultAttemptResponse) o;

        return new EqualsBuilder().append(resultAttempt, that.resultAttempt).append(correct, that.correct)
                                  .append(id, that.id).append(user, that.user)
                                  .append(multiplication, that.multiplication).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(id).append(user).append(multiplication).append(resultAttempt)
                                          .append(correct).toHashCode();
    }

    public static class Builder {

        private final MultiplicationResultAttemptResponse object;

        public Builder() {
            object = new MultiplicationResultAttemptResponse();
        }


        public Builder withId(Long value) {
            object.id = value;
            return this;
        }

        public Builder withUser(UserResponse userResponse) {
            object.user = userResponse;
            return this;
        }

        public Builder withMultiplication(MultiplicationResponse value) {
            object.multiplication = value;
            return this;
        }

        public Builder withResultAttempt(int value) {
            object.resultAttempt = value;
            return this;
        }

        public Builder withCorrect(Boolean value) {
            object.correct = value;
            return this;
        }


        public MultiplicationResultAttemptResponse build() {
            return object;
        }
    }
}
