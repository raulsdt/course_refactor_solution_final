package com.kairosds.book.adapter.journal;

import com.kairosds.book.JournalProvider;
import com.kairosds.book.adapter.journal.dto.MultiplicationSolvedEvent;
import org.springframework.stereotype.Service;

@Service
public class JournalAdapter implements JournalProvider {

    private EventDispatcher eventDispatcher;

    public JournalAdapter(EventDispatcher eventDispatcher) {
        this.eventDispatcher = eventDispatcher;
    }

    public void send(final Long multiplicationResultAttemptId, final Long userId, final boolean correct) {
        MultiplicationSolvedEvent multiplicationSolvedEvent = new MultiplicationSolvedEvent.Builder().withUserId(userId)
                                                                                                     .withMultiplicationResultAttemptId(
                                                                                                             multiplicationResultAttemptId)
                                                                                                     .withCorrect(
                                                                                                             correct)
                                                                                                     .build();

        eventDispatcher.send(multiplicationSolvedEvent);
    }
}
