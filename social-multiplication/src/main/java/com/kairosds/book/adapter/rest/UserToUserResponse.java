package com.kairosds.book.adapter.rest;

import com.kairosds.book.adapter.rest.dto.UserResponse;
import com.kairosds.book.domain.User;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class UserToUserResponse implements Converter<User, UserResponse> {
    @Override
    public UserResponse convert(User user) {
        return new UserResponse.Builder().withId(user.getId()).withAlias(user.getAlias()).build();
    }
}
