package com.kairosds.book.adapter.rest;

import com.kairosds.book.UserQueryService;
import com.kairosds.book.UserRepository;
import com.kairosds.book.domain.User;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/users")
public class UserController {

    private final UserQueryService userQueryService;

    public UserController(UserQueryService userQueryService) {
        this.userQueryService = userQueryService;
    }

    @GetMapping("/{userId}")
    public User getUserById(@PathVariable("userId") final Long userId) {
        return userQueryService.getUser(userId);
    }
}
