package com.kairosds.book.adapter.rest;

import com.kairosds.book.adapter.rest.dto.MultiplicationResultAttemptResponse;
import com.kairosds.book.domain.MultiplicationResultAttempt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class MultiplicationResultAttemptToMultiplciationResultAttemptResponseConverter implements Converter<MultiplicationResultAttempt, MultiplicationResultAttemptResponse> {

    private MultiplicationToMultiplicationResponseConverter multiplicationToMultiplicationResponseConverter;
    private UserToUserResponse userToUserResponse;

    @Autowired
    public MultiplicationResultAttemptToMultiplciationResultAttemptResponseConverter(
            MultiplicationToMultiplicationResponseConverter multiplicationToMultiplicationResponseConverter,
            UserToUserResponse userToUserResponse) {
        this.multiplicationToMultiplicationResponseConverter = multiplicationToMultiplicationResponseConverter;
        this.userToUserResponse = userToUserResponse;
    }

    @Override
    public MultiplicationResultAttemptResponse convert(MultiplicationResultAttempt multiplicationResultAttempt) {
        return new MultiplicationResultAttemptResponse.Builder().withId(multiplicationResultAttempt.getId())
                                                                .withMultiplication(
                                                                        multiplicationToMultiplicationResponseConverter
                                                                                .convert(multiplicationResultAttempt
                                                                                                 .getMultiplication()))
                                                                .withUser(userToUserResponse.convert(
                                                                        multiplicationResultAttempt.getUser()))
                                                                .withResultAttempt(
                                                                        multiplicationResultAttempt.getResultAttempt())
                                                                .withCorrect(multiplicationResultAttempt.isCorrect())
                                                                .build();
    }
}
