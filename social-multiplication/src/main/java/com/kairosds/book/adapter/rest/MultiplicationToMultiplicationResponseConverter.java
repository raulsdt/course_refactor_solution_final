package com.kairosds.book.adapter.rest;

import com.kairosds.book.adapter.rest.dto.MultiplicationResponse;
import com.kairosds.book.domain.Multiplication;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class MultiplicationToMultiplicationResponseConverter implements Converter<Multiplication,
        MultiplicationResponse> {

    @Override
    public MultiplicationResponse convert(Multiplication multiplication) {
        return new MultiplicationResponse.Builder().withId(multiplication.getId())
                                                   .withFactorA(multiplication.getFactorA())
                                                   .withFactorB(multiplication.getFactorB()).build();
    }
}
