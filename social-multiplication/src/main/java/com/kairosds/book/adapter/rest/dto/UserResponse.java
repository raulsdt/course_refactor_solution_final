package com.kairosds.book.adapter.rest.dto;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

public class UserResponse {

    private Long id;
    private String alias;

    public UserResponse() {
    }

    public UserResponse(Long id, String alias) {
        this.id = id;
        this.alias = alias;
    }

    public Long getId() {
        return id;
    }

    public String getAlias() {
        return alias;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        UserResponse that = (UserResponse) o;

        return new EqualsBuilder().append(id, that.id).append(alias, that.alias).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(id).append(alias).toHashCode();
    }

    public static class Builder {

        private final UserResponse object;

        public Builder() {
            object = new UserResponse();
        }


        public Builder withId(Long value) {
            object.id = value;
            return this;
        }

        public Builder withAlias(String value) {
            object.alias = value;
            return this;
        }

        public UserResponse build() {
            return object;
        }
    }
}
