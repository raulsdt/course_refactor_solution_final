package com.kairosds.book.adapter.rest.dto;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

public class MultiplicationResponse {

    private Long id;

    private int factorA;
    private int factorB;


    public MultiplicationResponse() {
    }

    public MultiplicationResponse(Long id, int factorA, int factorB) {
        this.id = id;
        this.factorA = factorA;
        this.factorB = factorB;
    }

    public Long getId() {
        return id;
    }

    public int getFactorA() {
        return factorA;
    }

    public int getFactorB() {
        return factorB;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        MultiplicationResponse that = (MultiplicationResponse) o;

        return new EqualsBuilder().append(factorA, that.factorA).append(factorB, that.factorB).append(id, that.id)
                                  .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(id).append(factorA).append(factorB).toHashCode();
    }

    public static class Builder {

        private final MultiplicationResponse object;

        public Builder() {
            object = new MultiplicationResponse();
        }


        public Builder withId(Long value) {
            object.id = value;
            return this;
        }

        public Builder withFactorA(int value) {
            object.factorA = value;
            return this;
        }

        public Builder withFactorB(int value) {
            object.factorB = value;
            return this;
        }


        public MultiplicationResponse build() {
            return object;
        }
    }
}
