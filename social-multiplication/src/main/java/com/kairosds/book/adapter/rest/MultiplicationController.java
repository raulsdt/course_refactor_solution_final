package com.kairosds.book.adapter.rest;

import com.kairosds.book.MultiplicationCommandService;
import com.kairosds.book.adapter.rest.dto.MultiplicationResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/multiplications")
public class MultiplicationController {

    private final MultiplicationCommandService multiplicationCommandService;
    private final MultiplicationToMultiplicationResponseConverter multiplicationToMultiplicationResponseConverter;

    @Autowired
    public MultiplicationController(final MultiplicationCommandService multiplicationCommandService,
            MultiplicationToMultiplicationResponseConverter multiplicationToMultiplicationResponseConverter) {
        this.multiplicationCommandService = multiplicationCommandService;
        this.multiplicationToMultiplicationResponseConverter = multiplicationToMultiplicationResponseConverter;
    }

    @GetMapping("/random")
    MultiplicationResponse getRandomMultiplication() {
        return multiplicationToMultiplicationResponseConverter
                .convert(multiplicationCommandService.createRandomMultiplication());
    }

}
