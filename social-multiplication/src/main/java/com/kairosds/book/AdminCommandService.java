package com.kairosds.book;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;


@Profile("test")
@Service
public class AdminCommandService {

    private MultiplicationRepository multiplicationRepository;
    private MultiplicationResultAttemptRepository attemptRepository;
    private UserRepository userRepository;

    public AdminCommandService(final MultiplicationRepository multiplicationRepository,
                            final UserRepository userRepository,
                            final MultiplicationResultAttemptRepository attemptRepository) {
        this.multiplicationRepository = multiplicationRepository;
        this.userRepository = userRepository;
        this.attemptRepository = attemptRepository;
    }

    public void deleteDatabaseContents() {
        attemptRepository.deleteAll();
        multiplicationRepository.deleteAll();
        userRepository.deleteAll();
    }
}
