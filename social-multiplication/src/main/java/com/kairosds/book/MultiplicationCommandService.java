package com.kairosds.book;

import com.kairosds.book.domain.Multiplication;
import com.kairosds.book.domain.MultiplicationResultAttempt;
import com.kairosds.book.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import javax.transaction.Transactional;
import java.util.Optional;

@Service
public class MultiplicationCommandService {

    private RandomGeneratorCommandService randomGeneratorService;
    private MultiplicationResultAttemptRepository attemptRepository;
    private UserRepository userRepository;
    private JournalProvider journalProvider;

    @Autowired
    public MultiplicationCommandService(final RandomGeneratorCommandService randomGeneratorService,
            final MultiplicationResultAttemptRepository attemptRepository, final UserRepository userRepository,
            final JournalProvider journalProvider) {
        this.randomGeneratorService = randomGeneratorService;
        this.attemptRepository = attemptRepository;
        this.userRepository = userRepository;
        this.journalProvider = journalProvider;
    }

    public Multiplication createRandomMultiplication() {
        int factorA = randomGeneratorService.generateRandomFactor();
        int factorB = randomGeneratorService.generateRandomFactor();
        return new Multiplication(factorA, factorB);
    }

    @Transactional
    public MultiplicationResultAttempt checkAttempt(final MultiplicationResultAttempt attempt) {
        Optional<User> user = userRepository.findByAlias(attempt.getUser().getAlias());

        Assert.isTrue(!attempt.isCorrect(), "You can't send an attempt marked as correct!!");

        boolean isCorrect = attempt.getResultAttempt() == attempt.getMultiplication().getFactorA() * attempt
                .getMultiplication().getFactorB();


        MultiplicationResultAttempt storedAttempt = attemptRepository
                .save(new MultiplicationResultAttempt(user.orElse(attempt.getUser()), attempt.getMultiplication(),
                                                      attempt.getResultAttempt(), isCorrect));

        journalProvider.send(storedAttempt.getId(), storedAttempt.getUser().getId(), storedAttempt.isCorrect());

        return storedAttempt;
    }


}
