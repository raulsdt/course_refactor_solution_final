package com.kairosds.book;

import org.springframework.stereotype.Service;

import java.security.SecureRandom;

@Service
public class RandomGeneratorCommandService {

    final static int MINIMUM_FACTOR = 11;
    final static int MAXIMUM_FACTOR = 99;

    public int generateRandomFactor() {
        return new SecureRandom().nextInt((MAXIMUM_FACTOR - MINIMUM_FACTOR) + 1) + MINIMUM_FACTOR;
    }
}
