package com.kairosds.book.service;

import com.kairosds.book.*;
import com.kairosds.book.adapter.journal.dto.MultiplicationSolvedEvent;
import com.kairosds.book.domain.Multiplication;
import com.kairosds.book.domain.MultiplicationResultAttempt;
import com.kairosds.book.domain.User;
import org.assertj.core.util.Lists;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;

public class MultiplicationCommandServiceTest {

    private MultiplicationCommandService multiplicationServiceImpl;
    private MultiplicationQueryService multiplicationQueryService;

    @Mock
    private RandomGeneratorCommandService randomGeneratorService;

    @Mock
    private MultiplicationResultAttemptRepository attemptRepository;

    @Mock
    private UserRepository userRepository;

    @Mock
    private JournalProvider journalProvider;

    @Before
    public void setUp() {
        // With this call to initMocks we tell Mockito to process the annotations
        MockitoAnnotations.initMocks(this);
        multiplicationServiceImpl = new MultiplicationCommandService(randomGeneratorService, attemptRepository,
                                                                     userRepository, journalProvider);
        multiplicationQueryService = new MultiplicationQueryService(attemptRepository);
    }

    @Test
    public void createRandomMultiplicationTest() {
        // given (our mocked Random Generator service will return first 50, then 30)
        given(randomGeneratorService.generateRandomFactor()).willReturn(50, 30);

        // when
        Multiplication multiplication = multiplicationServiceImpl.createRandomMultiplication();

        // then
        assertThat(multiplication.getFactorA()).isEqualTo(50);
        assertThat(multiplication.getFactorB()).isEqualTo(30);
    }

    @Test
    public void checkCorrectAttemptTest() {
        // given
        Multiplication multiplication = new Multiplication(50, 60);
        User user = new User("john_doe");
        MultiplicationResultAttempt attempt = new MultiplicationResultAttempt(user, multiplication, 3000, false);
        MultiplicationResultAttempt verifiedAttempt = new MultiplicationResultAttempt(user, multiplication, 3000, true);
        given(userRepository.findByAlias("john_doe")).willReturn(Optional.empty());
        // Note: the service will set correct to true
        given(attemptRepository.save(verifiedAttempt)).willReturn(verifiedAttempt);

        // when
        MultiplicationResultAttempt resultAttempt = multiplicationServiceImpl.checkAttempt(attempt);

        // then
        assertThat(resultAttempt.isCorrect()).isTrue();
        verify(attemptRepository).save(verifiedAttempt);
        verify(journalProvider).send(eq(attempt.getId()), eq(attempt.getUser().getId()), eq(true));
    }

    @Test
    public void checkWrongAttemptTest() {
        // given
        Multiplication multiplication = new Multiplication(50, 60);
        User user = new User("john_doe");
        MultiplicationResultAttempt attempt = new MultiplicationResultAttempt(user, multiplication, 3010, false);
        MultiplicationResultAttempt storedAttempt = new MultiplicationResultAttempt(user, multiplication, 3010, false);
        MultiplicationSolvedEvent event = new MultiplicationSolvedEvent(attempt.getId(), attempt.getUser().getId(),
                                                                        false);
        given(userRepository.findByAlias("john_doe")).willReturn(Optional.empty());
        given(attemptRepository.save(attempt)).willReturn(storedAttempt);

        // when
        MultiplicationResultAttempt resultAttempt = multiplicationServiceImpl.checkAttempt(attempt);

        // then
        assertThat(resultAttempt.isCorrect()).isFalse();
        verify(attemptRepository).save(attempt);
        verify(journalProvider).send(eq(attempt.getId()), eq(attempt.getUser().getId()), eq(true));
    }

    @Test
    public void retrieveStatsTest() {
        // given
        Multiplication multiplication = new Multiplication(50, 60);
        User user = new User("john_doe");
        MultiplicationResultAttempt attempt1 = new MultiplicationResultAttempt(user, multiplication, 3010, false);
        MultiplicationResultAttempt attempt2 = new MultiplicationResultAttempt(user, multiplication, 3051, false);
        List<MultiplicationResultAttempt> latestAttempts = Lists.newArrayList(attempt1, attempt2);
        given(userRepository.findByAlias("john_doe")).willReturn(Optional.empty());
        given(attemptRepository.findTop5ByUserAliasOrderByIdDesc("john_doe")).willReturn(latestAttempts);

        // when
        List<MultiplicationResultAttempt> latestAttemptsResult = multiplicationQueryService.getStatsForUser("john_doe");

        // then
        assertThat(latestAttemptsResult).isEqualTo(latestAttempts);
    }
}