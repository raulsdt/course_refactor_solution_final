package com.kairosds.book;

import com.kairosds.book.domain.LeaderBoardRow;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LeaderBoardService {

    private ScoreCardProvider scoreCardProvider;

    public LeaderBoardService(ScoreCardProvider scoreCardProvider) {
        this.scoreCardProvider = scoreCardProvider;
    }

    public List<LeaderBoardRow> getCurrentLeaderBoard() {
        return scoreCardProvider.findFirst10();
    }
}
