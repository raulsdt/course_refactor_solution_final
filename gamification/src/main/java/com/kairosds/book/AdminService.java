package com.kairosds.book;

import com.kairosds.book.adapter.persistence.BadgeJPARepository;
import com.kairosds.book.adapter.persistence.ScoreCardRepository;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;


@Profile("test")
@Service
public class AdminService {

    private BadgeJPARepository badgeCardProvider;
    private ScoreCardRepository scoreCardProvider;

    public AdminService(final BadgeJPARepository badgeCardProvider,
            final ScoreCardRepository scoreCardProvider) {
        this.badgeCardProvider = badgeCardProvider;
        this.scoreCardProvider = scoreCardProvider;
    }

    public void deleteDatabaseContents() {
        scoreCardProvider.deleteAll();
        badgeCardProvider.deleteAll();
    }
}
