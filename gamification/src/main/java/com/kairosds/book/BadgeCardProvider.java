package com.kairosds.book;

import com.kairosds.book.domain.BadgeCard;

import java.util.List;

public interface BadgeCardProvider {

    List<BadgeCard> findByUserIdOrderByBadgeTimestampDesc(final Long userId);

    void save(BadgeCard badgeCard);
}
