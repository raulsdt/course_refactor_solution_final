package com.kairosds.book;

import com.kairosds.book.domain.MultiplicationResultAttempt;

public interface MultiplicationProvider {

    MultiplicationResultAttempt retrieveMultiplicationResultAttemptbyId(final Long multiplicationResultAttemptId);
}
