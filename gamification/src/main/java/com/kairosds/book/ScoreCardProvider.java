package com.kairosds.book;

import com.kairosds.book.domain.LeaderBoardRow;
import com.kairosds.book.domain.ScoreCard;

import java.util.List;

public interface ScoreCardProvider {


    Integer getTotalScoreForUser(final Long userId);

    List<LeaderBoardRow> findFirst10();

    List<ScoreCard> findByUserIdOrderByScoreTimestampDesc(final Long userId);

    ScoreCard findByAttemptId(final Long attemptId);

    void save (ScoreCard scoreCard);
}
