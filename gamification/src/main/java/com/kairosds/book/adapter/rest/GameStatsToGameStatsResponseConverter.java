package com.kairosds.book.adapter.rest;

import com.kairosds.book.adapter.rest.dto.GameStatsResponse;
import com.kairosds.book.domain.GameStats;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class GameStatsToGameStatsResponseConverter implements Converter<GameStats, GameStatsResponse> {
    @Override
    public GameStatsResponse convert(GameStats gameStats) {
        return new GameStatsResponse.Builder().withUserId(gameStats.getUserId()).withScore(gameStats.getScore())
                                              .withBadgeTypes(gameStats.getBadges()).build();
    }
}
