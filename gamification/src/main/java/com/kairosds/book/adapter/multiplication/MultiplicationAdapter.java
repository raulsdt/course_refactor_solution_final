package com.kairosds.book.adapter.multiplication;

import com.kairosds.book.MultiplicationProvider;
import com.kairosds.book.adapter.multiplication.client.MultiplicationResultAttemptClient;
import com.kairosds.book.domain.MultiplicationResultAttempt;
import org.springframework.stereotype.Service;

@Service
public class MultiplicationAdapter implements MultiplicationProvider {

    private MultiplicationResultAttemptClient multiplicationResultAttemptClient;

    public MultiplicationAdapter(MultiplicationResultAttemptClient multiplicationResultAttemptClient) {
        this.multiplicationResultAttemptClient = multiplicationResultAttemptClient;
    }

    public MultiplicationResultAttempt retrieveMultiplicationResultAttemptbyId(
            final Long multiplicationResultAttemptId) {
        return multiplicationResultAttemptClient.retrieveMultiplicationResultAttemptbyId(multiplicationResultAttemptId);
    }
}
