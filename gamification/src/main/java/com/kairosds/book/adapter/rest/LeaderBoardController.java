package com.kairosds.book.adapter.rest;

import com.kairosds.book.LeaderBoardService;
import com.kairosds.book.adapter.rest.dto.LeaderBoardRowResponse;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/leaders")
public class LeaderBoardController {

    private final LeaderBoardService leaderBoardService;
    private final LeaderBoardRowToLeaderBoardRowResponseConverter leaderBoardRowToLeaderBoardRowResponseConverter;

    public LeaderBoardController(final LeaderBoardService leaderBoardService,
            LeaderBoardRowToLeaderBoardRowResponseConverter leaderBoardRowToLeaderBoardRowResponseConverter) {
        this.leaderBoardService = leaderBoardService;
        this.leaderBoardRowToLeaderBoardRowResponseConverter = leaderBoardRowToLeaderBoardRowResponseConverter;
    }

    @GetMapping
    public List<LeaderBoardRowResponse> getLeaderBoard() {
        return leaderBoardService.getCurrentLeaderBoard().stream()
                                 .map(leaderBoardRowToLeaderBoardRowResponseConverter::convert)
                                 .collect(Collectors.toList());
    }
}
