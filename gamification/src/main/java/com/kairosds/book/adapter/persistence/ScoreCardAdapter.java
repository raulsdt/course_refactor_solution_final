package com.kairosds.book.adapter.persistence;

import com.kairosds.book.ScoreCardProvider;
import com.kairosds.book.adapter.persistence.dto.ScoreCardEntity;
import com.kairosds.book.domain.LeaderBoardRow;
import com.kairosds.book.domain.ScoreCard;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ScoreCardAdapter implements ScoreCardProvider {

    private final ScoreCardRepository scoreCardRepository;
    private final ConversionService conversionService;

    public ScoreCardAdapter(ScoreCardRepository scoreCardRepository, ConversionService conversionService) {
        this.scoreCardRepository = scoreCardRepository;
        this.conversionService = conversionService;
    }

    public Integer getTotalScoreForUser(final Long userId) {
        return scoreCardRepository.getTotalScoreForUser(userId);
    }

    public List<LeaderBoardRow> findFirst10() {
        return scoreCardRepository.findFirst10();
    }

    public List<ScoreCard> findByUserIdOrderByScoreTimestampDesc(final Long userId) {
        return scoreCardRepository.findByUserIdOrderByScoreTimestampDesc(userId).stream()
                                  .map(scoreCardEntity -> conversionService
                                             .convert(scoreCardEntity, ScoreCard.class)).collect(Collectors.toList());
    }

    public ScoreCard findByAttemptId(final Long attemptId){
        return conversionService.convert(scoreCardRepository.findByAttemptId(attemptId), ScoreCard.class);
    }

    @Override
    public void save(ScoreCard scoreCard) {
        scoreCardRepository.save(conversionService.convert(scoreCard, ScoreCardEntity.class));
    }


}
