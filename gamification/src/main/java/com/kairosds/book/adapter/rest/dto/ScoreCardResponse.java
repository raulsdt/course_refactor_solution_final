package com.kairosds.book.adapter.rest.dto;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

public class ScoreCardResponse {

    public static final int DEFAULT_SCORE = 10;

    private Long cardId;
    private Long userId;
    private Long attemptId;
    private long scoreTimestamp;
    private int score;

    public ScoreCardResponse(Long cardId, Long userId, Long attemptId, long scoreTimestamp, int score) {
        this.cardId = cardId;
        this.userId = userId;
        this.attemptId = attemptId;
        this.scoreTimestamp = scoreTimestamp;
        this.score = score;
    }

    public ScoreCardResponse() {
    }

    public static int getDefaultScore() {
        return DEFAULT_SCORE;
    }

    public Long getCardId() {
        return cardId;
    }

    public Long getUserId() {
        return userId;
    }

    public Long getAttemptId() {
        return attemptId;
    }

    public long getScoreTimestamp() {
        return scoreTimestamp;
    }

    public int getScore() {
        return score;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        ScoreCardResponse that = (ScoreCardResponse) o;

        return new EqualsBuilder().append(scoreTimestamp, that.scoreTimestamp).append(score, that.score)
                                  .append(cardId, that.cardId).append(userId, that.userId)
                                  .append(attemptId, that.attemptId).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(cardId).append(userId).append(attemptId).append(scoreTimestamp)
                                          .append(score).toHashCode();
    }

    public static class Builder {

        private final ScoreCardResponse object;

        public Builder() {
            object = new ScoreCardResponse();
        }


        public Builder withCardId(Long value) {
            object.cardId = value;
            return this;
        }

        public Builder withUserId(Long value) {
            object.userId = value;
            return this;
        }

        public Builder withAttemptId(Long value) {
            object.attemptId = value;
            return this;
        }

        public Builder withScoreTimeStamp(Long value) {
            object.scoreTimestamp = value;
            return this;
        }

        public Builder withScore(int value) {
            object.score = value;
            return this;
        }


        public ScoreCardResponse build() {
            return object;
        }
    }
}
