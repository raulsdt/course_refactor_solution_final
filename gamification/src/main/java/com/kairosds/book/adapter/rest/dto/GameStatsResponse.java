package com.kairosds.book.adapter.rest.dto;

import com.kairosds.book.domain.Badge;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import java.util.List;

public class GameStatsResponse {

    private Long userId;
    private int score;
    private List<Badge> badges;

    public GameStatsResponse() {
    }

    public GameStatsResponse(Long userId, int score, List<Badge> badges) {
        this.userId = userId;
        this.score = score;
        this.badges = badges;
    }

    public Long getUserId() {
        return userId;
    }

    public int getScore() {
        return score;
    }

    public List<Badge> getBadges() {
        return badges;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        GameStatsResponse that = (GameStatsResponse) o;

        return new EqualsBuilder().append(score, that.score).append(userId, that.userId)
                                  .append(badges, that.badges).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(userId).append(score).append(badges).toHashCode();
    }

    public static class Builder {

        private final GameStatsResponse object;

        public Builder() {
            object = new GameStatsResponse();
        }


        public Builder withUserId(Long value) {
            object.userId = value;
            return this;
        }

        public Builder withScore(int value) {
            object.score = value;
            return this;
        }

        public Builder withBadgeTypes(List<Badge> value) {
            object.badges = value;
            return this;
        }


        public GameStatsResponse build() {
            return object;
        }
    }
}
