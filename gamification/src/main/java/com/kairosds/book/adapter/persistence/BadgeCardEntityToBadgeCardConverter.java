package com.kairosds.book.adapter.persistence;

import com.kairosds.book.adapter.persistence.dto.BadgeCardEntity;
import com.kairosds.book.domain.BadgeCard;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class BadgeCardEntityToBadgeCardConverter implements Converter<BadgeCardEntity, BadgeCard> {
    @Override
    public BadgeCard convert(BadgeCardEntity badgeCardEntity) {
        return new BadgeCard(badgeCardEntity.getBadgeId(), badgeCardEntity.getUserId(),
                             badgeCardEntity.getBadgeTimestamp(), badgeCardEntity.getBadge());
    }
}
