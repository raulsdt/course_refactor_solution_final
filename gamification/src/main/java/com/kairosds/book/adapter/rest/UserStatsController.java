package com.kairosds.book.adapter.rest;

import com.kairosds.book.GameService;
import com.kairosds.book.adapter.rest.dto.GameStatsResponse;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/stats")
public class UserStatsController {

    private final GameService gameService;
    private final GameStatsToGameStatsResponseConverter gameStatsToGameStatsResponseConverter;

    public UserStatsController(final GameService gameService,
            final GameStatsToGameStatsResponseConverter gameStatsToGameStatsResponseConverter) {
        this.gameService = gameService;
        this.gameStatsToGameStatsResponseConverter = gameStatsToGameStatsResponseConverter;
    }

    @GetMapping
    public GameStatsResponse getStatsForUser(@RequestParam("userId") final Long userId) {
        return gameStatsToGameStatsResponseConverter.convert(gameService.retrieveStatsForUser(userId));
    }
}
