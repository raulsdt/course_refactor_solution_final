package com.kairosds.book.adapter.persistence;

import com.kairosds.book.adapter.persistence.dto.ScoreCardEntity;
import com.kairosds.book.domain.ScoreCard;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class ScoreCardToScoreCardEntityConverter implements Converter<ScoreCard, ScoreCardEntity> {
    @Override
    public ScoreCardEntity convert(ScoreCard scoreCard) {
        return new ScoreCardEntity(scoreCard.getCardId(), scoreCard.getUserId(), scoreCard.getAttemptId(),
                                   scoreCard.getScoreTimestamp(), scoreCard.getScore());
    }
}
