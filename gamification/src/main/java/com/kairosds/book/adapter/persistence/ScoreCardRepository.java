package com.kairosds.book.adapter.persistence;

import com.kairosds.book.domain.LeaderBoardRow;
import com.kairosds.book.adapter.persistence.dto.ScoreCardEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ScoreCardRepository extends CrudRepository<ScoreCardEntity, Long> {


    @Query("SELECT SUM(s.score) FROM com.kairosds.book.domain.ScoreCard s WHERE s.userId = :userId " +
            "GROUP BY s.userId")
    Integer getTotalScoreForUser(@Param("userId") final Long userId);

    @Query("SELECT NEW com.kairosds.book.domain.LeaderBoardRow(s.userId, SUM(s.score)) " + "FROM " +
            "com.kairosds.book.domain.ScoreCard s " + "GROUP BY s.userId ORDER BY SUM(s.score) DESC")
    List<LeaderBoardRow> findFirst10();

    List<ScoreCardEntity> findByUserIdOrderByScoreTimestampDesc(final Long userId);

    ScoreCardEntity findByAttemptId(final Long attemptId);
}
