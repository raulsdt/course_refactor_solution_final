package com.kairosds.book.adapter.persistence.dto;

import com.kairosds.book.domain.Badge;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public final class BadgeCardEntity {

    @Id
    @GeneratedValue
    @Column(name = "BADGE_ID")
    private final Long badgeId;

    private final Long userId;
    private final long badgeTimestamp;
    private Badge badge;

    // Empty constructor for JSON / JPA
    public BadgeCardEntity() {
        this(null, null, 0, null);
    }

    public BadgeCardEntity(final Long userId, final Badge badge) {
        this(null, userId, System.currentTimeMillis(), badge);
    }

    public BadgeCardEntity(Long badgeId, Long userId, long badgeTimestamp, Badge badge) {
        this.badgeId = badgeId;
        this.userId = userId;
        this.badgeTimestamp = badgeTimestamp;
        this.badge = badge;
    }

    public Long getBadgeId() {
        return badgeId;
    }

    public Long getUserId() {
        return userId;
    }

    public long getBadgeTimestamp() {
        return badgeTimestamp;
    }

    public Badge getBadge() {
        return badge;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        BadgeCardEntity badgeCardEntity = (BadgeCardEntity) o;

        return new EqualsBuilder().append(badgeTimestamp, badgeCardEntity.badgeTimestamp).append(badgeId, badgeCardEntity.badgeId)
                                  .append(userId, badgeCardEntity.userId).append(badge, badgeCardEntity.badge).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(badgeId).append(userId).append(badgeTimestamp).append(badge)
                                          .toHashCode();
    }
}
