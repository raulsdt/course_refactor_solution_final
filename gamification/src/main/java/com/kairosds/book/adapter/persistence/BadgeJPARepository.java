package com.kairosds.book.adapter.persistence;

import com.kairosds.book.adapter.persistence.dto.BadgeCardEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface BadgeJPARepository extends CrudRepository<BadgeCardEntity, Long> {

    List<BadgeCardEntity> findByUserIdOrderByBadgeTimestampDesc(final Long userId);

}
