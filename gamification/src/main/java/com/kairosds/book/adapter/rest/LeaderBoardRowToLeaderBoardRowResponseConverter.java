package com.kairosds.book.adapter.rest;

import com.kairosds.book.adapter.rest.dto.LeaderBoardRowResponse;
import com.kairosds.book.domain.LeaderBoardRow;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class LeaderBoardRowToLeaderBoardRowResponseConverter implements Converter<LeaderBoardRow,
        LeaderBoardRowResponse> {
    @Override
    public LeaderBoardRowResponse convert(LeaderBoardRow leaderBoardRow) {
        return new LeaderBoardRowResponse.Builder().withUserId(leaderBoardRow.getUserId())
                                                   .withTotalScore(leaderBoardRow.getTotalScore()).build();
    }
}
