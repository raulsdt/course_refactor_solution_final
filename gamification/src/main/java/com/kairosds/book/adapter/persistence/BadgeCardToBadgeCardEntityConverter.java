package com.kairosds.book.adapter.persistence;

import com.kairosds.book.adapter.persistence.dto.BadgeCardEntity;
import com.kairosds.book.domain.BadgeCard;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class BadgeCardToBadgeCardEntityConverter implements Converter<BadgeCard, BadgeCardEntity> {
    @Override
    public BadgeCardEntity convert(BadgeCard badgeCard) {
        return new BadgeCardEntity(badgeCard.getBadgeId(), badgeCard.getUserId(), badgeCard.getBadgeTimestamp(),
                                   badgeCard.getBadge());
    }
}
