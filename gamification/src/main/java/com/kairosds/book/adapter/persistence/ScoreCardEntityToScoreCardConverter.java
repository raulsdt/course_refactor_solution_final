package com.kairosds.book.adapter.persistence;

import com.kairosds.book.adapter.persistence.dto.ScoreCardEntity;
import com.kairosds.book.domain.ScoreCard;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class ScoreCardEntityToScoreCardConverter implements Converter<ScoreCardEntity, ScoreCard> {
    @Override
    public ScoreCard convert(ScoreCardEntity scoreCardEntity) {
        return new ScoreCard(scoreCardEntity.getCardId(), scoreCardEntity.getUserId(), scoreCardEntity.getAttemptId(),
                             scoreCardEntity.getScoreTimestamp(), scoreCardEntity.getScore());
    }
}
