package com.kairosds.book.adapter.journal.dto;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import java.io.Serializable;

public class MultiplicationSolvedEvent implements Serializable {

    private Long multiplicationResultAttemptId;
    private Long userId;
    private boolean correct;

    public MultiplicationSolvedEvent() {
    }

    public MultiplicationSolvedEvent(Long multiplicationResultAttemptId, Long userId, boolean correct) {
        this.multiplicationResultAttemptId = multiplicationResultAttemptId;
        this.userId = userId;
        this.correct = correct;
    }

    public Long getMultiplicationResultAttemptId() {
        return multiplicationResultAttemptId;
    }

    public Long getUserId() {
        return userId;
    }

    public boolean isCorrect() {
        return correct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        MultiplicationSolvedEvent that = (MultiplicationSolvedEvent) o;

        return new EqualsBuilder().append(correct, that.correct)
                                  .append(multiplicationResultAttemptId, that.multiplicationResultAttemptId)
                                  .append(userId, that.userId).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(multiplicationResultAttemptId).append(userId).append(correct)
                                          .toHashCode();
    }

    public static class Builder {

        private final MultiplicationSolvedEvent object;

        public Builder() {
            object = new MultiplicationSolvedEvent();
        }


        public Builder withUserId(Long value) {
            object.userId = value;
            return this;
        }

        public Builder withMultiplicationResultAttemptId(Long value) {
            object.multiplicationResultAttemptId = value;
            return this;
        }

        public Builder withCorrect(Boolean value) {
            object.correct = value;
            return this;
        }


        public MultiplicationSolvedEvent build() {
            return object;
        }
    }
}
