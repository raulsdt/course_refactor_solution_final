package com.kairosds.book.adapter.persistence;

import com.kairosds.book.BadgeCardProvider;
import com.kairosds.book.adapter.persistence.dto.BadgeCardEntity;
import com.kairosds.book.domain.BadgeCard;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class BadgeCardAdapter implements BadgeCardProvider {

    private final BadgeJPARepository badgeJPARepository;
    private final ConversionService conversionService;

    public BadgeCardAdapter(BadgeJPARepository badgeJPARepository, ConversionService conversionService) {
        this.badgeJPARepository = badgeJPARepository;
        this.conversionService = conversionService;
    }


    public List<BadgeCard> findByUserIdOrderByBadgeTimestampDesc(final Long userId) {
        List<BadgeCardEntity> badgeCardEntities = badgeJPARepository.findByUserIdOrderByBadgeTimestampDesc(userId);
        return badgeCardEntities.stream()
                                .map(badgeCardEntity -> conversionService.convert(badgeCardEntity, BadgeCard.class))
                                .collect(Collectors.toList());
    }


    public void save(BadgeCard badgeCard){
        badgeJPARepository.save(conversionService.convert(badgeCard, BadgeCardEntity.class));
    }
}
