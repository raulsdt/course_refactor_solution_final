package com.kairosds.book.adapter.rest;

import com.kairosds.book.adapter.rest.dto.ScoreCardResponse;
import com.kairosds.book.domain.ScoreCard;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class ScoreCardToScoreCardResponseConverter implements Converter<ScoreCard, ScoreCardResponse> {
    @Override
    public ScoreCardResponse convert(ScoreCard scoreCard) {
        if (scoreCard != null) {

            return new ScoreCardResponse.Builder().withCardId(scoreCard.getCardId())
                                                  .withAttemptId(scoreCard.getAttemptId())
                                                  .withScore(scoreCard.getScore())
                                                  .withScoreTimeStamp(scoreCard.getScoreTimestamp())
                                                  .withUserId(scoreCard.getUserId()).build();


        } else {
            return new ScoreCardResponse.Builder().build();
        }
    }
}
