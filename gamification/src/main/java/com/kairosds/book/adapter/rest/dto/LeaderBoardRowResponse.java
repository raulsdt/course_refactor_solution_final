package com.kairosds.book.adapter.rest.dto;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

public final class LeaderBoardRowResponse {

    private Long userId;
    private Long totalScore;

    // Empty constructor for JSON / JPA
    public LeaderBoardRowResponse() {
        this(0L, 0L);
    }

    public LeaderBoardRowResponse(Long userId, Long totalScore) {
        this.userId = userId;
        this.totalScore = totalScore;
    }

    public Long getUserId() {
        return userId;
    }

    public Long getTotalScore() {
        return totalScore;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        LeaderBoardRowResponse that = (LeaderBoardRowResponse) o;

        return new EqualsBuilder().append(userId, that.userId).append(totalScore, that.totalScore).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(userId).append(totalScore).toHashCode();
    }

    public static class Builder {

        private final LeaderBoardRowResponse object;

        public Builder() {
            object = new LeaderBoardRowResponse();
        }


        public Builder withUserId(Long value) {
            object.userId = value;
            return this;
        }

        public Builder withTotalScore(Long value) {
            object.totalScore = value;
            return this;
        }


        public LeaderBoardRowResponse build() {
            return object;
        }
    }
}
