package com.kairosds.book.adapter.rest;

import com.kairosds.book.GameService;
import com.kairosds.book.adapter.rest.dto.ScoreCardResponse;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/scores")
public class ScoreController {

    private final GameService gameService;
    private final ScoreCardToScoreCardResponseConverter scoreCardToScoreCardResponseConverter;

    public ScoreController(final GameService gameService,
            ScoreCardToScoreCardResponseConverter scoreCardToScoreCardResponseConverter) {
        this.gameService = gameService;
        this.scoreCardToScoreCardResponseConverter = scoreCardToScoreCardResponseConverter;
    }

    @GetMapping("/{attemptId}")
    public ScoreCardResponse getScoreForAttempt(@PathVariable("attemptId") final Long attemptId) {
        return scoreCardToScoreCardResponseConverter.convert(gameService.getScoreForAttempt(attemptId));
    }
}
