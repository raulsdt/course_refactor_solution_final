package com.kairosds.book.adapter.multiplication.client;

import com.kairosds.book.domain.MultiplicationResultAttempt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;


@Component
public class MultiplicationResultAttemptClient {

    private RestTemplate restTemplate;
    private String multiplicationHost;

    @Autowired
    public MultiplicationResultAttemptClient(final RestTemplate restTemplate,
            @Value("${multiplicationHost}") final String multiplicationHost) {
        this.restTemplate = restTemplate;
        this.multiplicationHost = multiplicationHost;
    }

    public MultiplicationResultAttempt retrieveMultiplicationResultAttemptbyId(
            final Long multiplicationResultAttemptId) {
        return restTemplate.getForObject(multiplicationHost + "/results/" + multiplicationResultAttemptId,
                                         MultiplicationResultAttempt.class);
    }

    public MultiplicationResultAttempt defaultResult(final Long multiplicationResultAttemptId) {
        return new MultiplicationResultAttempt("fakeAlias", 10, 10, 100, true);
    }
}
