package com.kairosds.book.domain;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.kairosds.book.adapter.multiplication.dto.MultiplicationResultAttemptDeserializer;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

@JsonDeserialize(using = MultiplicationResultAttemptDeserializer.class)
public class MultiplicationResultAttempt {

    private final String userAlias;

    private final int multiplicationFactorA;
    private final int multiplicationFactorB;
    private final int resultAttempt;

    private final boolean correct;

    // Empty constructor for JSON/JPA
    public MultiplicationResultAttempt() {
        userAlias = null;
        multiplicationFactorA = -1;
        multiplicationFactorB = -1;
        resultAttempt = -1;
        correct = false;
    }

    public MultiplicationResultAttempt(String userAlias, int multiplicationFactorA, int multiplicationFactorB,
            int resultAttempt, boolean correct) {
        this.userAlias = userAlias;
        this.multiplicationFactorA = multiplicationFactorA;
        this.multiplicationFactorB = multiplicationFactorB;
        this.resultAttempt = resultAttempt;
        this.correct = correct;
    }

    public String getUserAlias() {
        return userAlias;
    }

    public int getMultiplicationFactorA() {
        return multiplicationFactorA;
    }

    public int getMultiplicationFactorB() {
        return multiplicationFactorB;
    }

    public int getResultAttempt() {
        return resultAttempt;
    }

    public boolean isCorrect() {
        return correct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        MultiplicationResultAttempt that = (MultiplicationResultAttempt) o;

        return new EqualsBuilder().append(multiplicationFactorA, that.multiplicationFactorA)
                                  .append(multiplicationFactorB, that.multiplicationFactorB)
                                  .append(resultAttempt, that.resultAttempt).append(correct, that.correct)
                                  .append(userAlias, that.userAlias).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(userAlias).append(multiplicationFactorA).append(multiplicationFactorB)
                                          .append(resultAttempt).append(correct).toHashCode();
    }
}
