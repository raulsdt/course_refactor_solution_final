package com.kairosds.book.domain;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

public final class BadgeCard {

    private final Long badgeId;
    private final Long userId;
    private final long badgeTimestamp;
    private Badge badge;

    // Empty constructor for JSON / JPA
    public BadgeCard() {
        this(null, null, 0, null);
    }

    public BadgeCard(final Long userId, final Badge badge) {
        this(null, userId, System.currentTimeMillis(), badge);
    }

    public BadgeCard(Long badgeId, Long userId, long badgeTimestamp, Badge badge) {
        this.badgeId = badgeId;
        this.userId = userId;
        this.badgeTimestamp = badgeTimestamp;
        this.badge = badge;
    }

    public Long getBadgeId() {
        return badgeId;
    }

    public Long getUserId() {
        return userId;
    }

    public long getBadgeTimestamp() {
        return badgeTimestamp;
    }

    public Badge getBadge() {
        return badge;
    }

    public void setBadge(Badge badge) {
        this.badge = badge;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        BadgeCard badgeCardEntity = (BadgeCard) o;

        return new EqualsBuilder().append(badgeTimestamp, badgeCardEntity.badgeTimestamp).append(badgeId, badgeCardEntity.badgeId)
                                  .append(userId, badgeCardEntity.userId).append(badge, badgeCardEntity.badge).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(badgeId).append(userId).append(badgeTimestamp).append(badge)
                                          .toHashCode();
    }
}
