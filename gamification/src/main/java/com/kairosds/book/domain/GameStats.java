package com.kairosds.book.domain;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class GameStats {

    private final Long userId;
    private final int score;
    private final List<Badge> badges;

    // Empty constructor for JSON / JPA
    public GameStats() {
        this.userId = 0L;
        this.score = 0;
        this.badges = new ArrayList<>();
    }

    public GameStats(Long userId, int score, List<Badge> badges) {
        this.userId = userId;
        this.score = score;
        this.badges = badges;
    }


    public Long getUserId() {
        return userId;
    }

    public int getScore() {
        return score;
    }

    public static GameStats emptyStats(final Long userId) {
        return new GameStats(userId, 0, Collections.emptyList());
    }

    public List<Badge> getBadges() {
        return Collections.unmodifiableList(badges);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        GameStats gameStats = (GameStats) o;

        return new EqualsBuilder().append(score, gameStats.score).append(userId, gameStats.userId)
                                  .append(badges, gameStats.badges).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(userId).append(score).append(badges).toHashCode();
    }
}
