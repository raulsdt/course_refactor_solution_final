package com.kairosds.book.domain;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;


public class ScoreCard {

    // The default score assigned to this card, if not specified.
    public static final int DEFAULT_SCORE = 10;


    private Long cardId;
    private Long userId;
    private Long attemptId;
    private long scoreTimestamp;
    private int score;

    // Empty constructor for JSON / JPA
    public ScoreCard() {
        this(null, null, null, 0, 0);
    }

    public ScoreCard(final Long userId, final Long attemptId) {
        this(null, userId, attemptId, System.currentTimeMillis(), DEFAULT_SCORE);
    }

    public ScoreCard(Long cardId, Long userId, Long attemptId, long scoreTimestamp, int score) {
        this.cardId = cardId;
        this.userId = userId;
        this.attemptId = attemptId;
        this.scoreTimestamp = scoreTimestamp;
        this.score = score;
    }

    public static int getDefaultScore() {
        return DEFAULT_SCORE;
    }

    public Long getCardId() {
        return cardId;
    }

    public Long getUserId() {
        return userId;
    }

    public Long getAttemptId() {
        return attemptId;
    }

    public long getScoreTimestamp() {
        return scoreTimestamp;
    }

    public int getScore() {
        return score;
    }

    public void setCardId(Long cardId) {
        this.cardId = cardId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public void setAttemptId(Long attemptId) {
        this.attemptId = attemptId;
    }

    public void setScoreTimestamp(long scoreTimestamp) {
        this.scoreTimestamp = scoreTimestamp;
    }

    public void setScore(int score) {
        this.score = score;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        ScoreCard scoreCardEntity = (ScoreCard) o;

        return new EqualsBuilder().append(scoreTimestamp, scoreCardEntity.scoreTimestamp).append(score, scoreCardEntity.score)
                                  .append(cardId, scoreCardEntity.cardId).append(userId, scoreCardEntity.userId)
                                  .append(attemptId, scoreCardEntity.attemptId).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(cardId).append(userId).append(attemptId).append(scoreTimestamp)
                                          .append(score).toHashCode();
    }
}
