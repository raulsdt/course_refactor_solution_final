package com.kairosds.book;


import com.kairosds.book.domain.*;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


@Service
public class GameService {

    public static final int LUCKY_NUMBER = 42;

    private ScoreCardProvider scoreCardProvider;
    private BadgeCardProvider badgeCardProvider;
    private MultiplicationProvider multiplicationProvider;

    public GameService(ScoreCardProvider scoreCardProvider, BadgeCardProvider badgeCardProvider,
            MultiplicationProvider multiplicationProvider) {
        this.scoreCardProvider = scoreCardProvider;
        this.badgeCardProvider = badgeCardProvider;
        this.multiplicationProvider = multiplicationProvider;
    }

    public GameStats newAttemptForUser(final Long userId, final Long attemptId, final boolean correct) {
        // For the first version we'll give points only if it's correct
        if (correct) {
            ScoreCard scoreCard = new ScoreCard(userId, attemptId);
            scoreCardProvider.save(scoreCard);
            List<BadgeCard> badgeCards = processForBadges(userId, attemptId);
            return new GameStats(userId, scoreCard.getScore(),
                                 badgeCards.stream().map(BadgeCard::getBadge).collect(Collectors.toList()));
        }
        return GameStats.emptyStats(userId);
    }

    private List<BadgeCard> processForBadges(final Long userId, final Long attemptId) {
        List<BadgeCard> badgeCards = new ArrayList<>();

        int totalScore = scoreCardProvider.getTotalScoreForUser(userId);

        List<ScoreCard> scoreCardList = scoreCardProvider.findByUserIdOrderByScoreTimestampDesc(userId);
        List<BadgeCard> badgeCardList = badgeCardProvider.findByUserIdOrderByBadgeTimestampDesc(userId);

        // Badges depending on score
        checkAndGiveBadgeBasedOnScore(badgeCardList, Badge.BRONZE_MULTIPLICATOR, totalScore, 100, userId)
                .ifPresent(badgeCards::add);
        checkAndGiveBadgeBasedOnScore(badgeCardList, Badge.SILVER_MULTIPLICATOR, totalScore, 500, userId)
                .ifPresent(badgeCards::add);
        checkAndGiveBadgeBasedOnScore(badgeCardList, Badge.GOLD_MULTIPLICATOR, totalScore, 999, userId)
                .ifPresent(badgeCards::add);

        // First won badge
        if (scoreCardList.size() == 1 && !containsBadge(badgeCardList, Badge.FIRST_WON)) {
            BadgeCard firstWonBadge = giveBadgeToUser(Badge.FIRST_WON, userId);
            badgeCards.add(firstWonBadge);
        }

        // Lucky number badge
        MultiplicationResultAttempt attempt = multiplicationProvider.retrieveMultiplicationResultAttemptbyId(attemptId);
        if (!containsBadge(badgeCardList, Badge.LUCKY_NUMBER) && (LUCKY_NUMBER == attempt
                .getMultiplicationFactorA() || LUCKY_NUMBER == attempt.getMultiplicationFactorB())) {
            BadgeCard luckyNumberBadge = giveBadgeToUser(Badge.LUCKY_NUMBER, userId);
            badgeCards.add(luckyNumberBadge);
        }

        return badgeCards;
    }

    public GameStats retrieveStatsForUser(final Long userId) {
        Integer score = scoreCardProvider.getTotalScoreForUser(userId);
        // If the user does not exist yet, it means it has 0 score
        if (score == null) {
            return new GameStats(userId, 0, Collections.emptyList());
        }
        List<BadgeCard> badgeCards = badgeCardProvider.findByUserIdOrderByBadgeTimestampDesc(userId);
        return new GameStats(userId, score, badgeCards.stream().map(BadgeCard::getBadge).collect(Collectors.toList()));
    }

    public ScoreCard getScoreForAttempt(final Long attemptId) {
        return scoreCardProvider.findByAttemptId(attemptId);
    }

    private Optional<BadgeCard> checkAndGiveBadgeBasedOnScore(final List<BadgeCard> badgeCards, final Badge badge,
            final int score, final int scoreThreshold, final Long userId) {
        if (score >= scoreThreshold && !containsBadge(badgeCards, badge)) {
            return Optional.of(giveBadgeToUser(badge, userId));
        }
        return Optional.empty();
    }

    private boolean containsBadge(final List<BadgeCard> badgeCards, final Badge badge) {
        return badgeCards.stream().anyMatch(b -> b.getBadge() == badge);
    }

    private BadgeCard giveBadgeToUser(final Badge badge, final Long userId) {
        BadgeCard badgeCard = new BadgeCard(userId, badge);
        badgeCardProvider.save(badgeCard);
        return badgeCard;
    }

}
